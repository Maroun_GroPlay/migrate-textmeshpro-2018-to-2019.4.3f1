﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TMPDataContainer 
{
    public List<TMPData> data;
}
