﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;

public class SaveTextMeshProEditor : EditorWindow
{
    private TMPData _tmpData;
    private List<TMPData> _tmpDataList;
    
    // Add menu named "My Window" to the Window menu
    [MenuItem("Migrate/TextMeshPro")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        SaveTextMeshProEditor window = (SaveTextMeshProEditor)EditorWindow.GetWindow(typeof(SaveTextMeshProEditor));
        window.Show();

    }
    
    public static GameObject[] GetAllObjectsInResources()
    {
        GameObject[] prefabs = Resources.LoadAll<GameObject>("");
        return prefabs;
    }


   public static List<GameObject> GetAllObjectsOnlyInScene()
    {
        List<GameObject> objectsInScene = new List<GameObject>();
 
        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (!EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
                objectsInScene.Add(go);
        }
 
        return objectsInScene;
    }



    void AddToList(TextMeshProUGUI textMeshProUGUI)
    {
        var gameObject = textMeshProUGUI.gameObject;
               if (gameObject.GetComponent<UniqueFixedID>() == null)
               {
                   gameObject.AddComponent<UniqueFixedID>();
               }

               var fixedID = gameObject.GetComponent<UniqueFixedID>();
               
               _tmpData = new TMPData();

               _tmpData.guid = fixedID.m_ID;
               _tmpData.color = textMeshProUGUI.color;
               _tmpData.margin = textMeshProUGUI.margin;
               _tmpData.text = textMeshProUGUI.text;
               _tmpData.alignmentOptions = textMeshProUGUI.alignment;
               _tmpData.autoSize = textMeshProUGUI.autoSizeTextContainer;
               _tmpData.characterSpacing = textMeshProUGUI.characterSpacing;
               _tmpData.fontSize = textMeshProUGUI.fontSize;
               _tmpData.lineSpacing = textMeshProUGUI.lineSpacing;
               _tmpData.paragraphSpacing = textMeshProUGUI.paragraphSpacing;
               _tmpData.wordSpacing = textMeshProUGUI.wordSpacing;
               _tmpData.currentFontAsset = textMeshProUGUI.font.name;
               
                
               /*
               Debug.Log("ID " + _tmpData.guid);
               Debug.Log("Color " +_tmpData.color);
               Debug.Log("Font " +_tmpData.font);
               Debug.Log("Margin  " +_tmpData.margin);
               Debug.Log("Text  " +_tmpData.text);

               Debug.Log("alignmentOptions  " +_tmpData.alignmentOptions);
               Debug.Log("autoSize  " +_tmpData.autoSize);
               Debug.Log("characterSpacing  " +_tmpData.characterSpacing);
               Debug.Log("fontSize  " +_tmpData.fontSize);
               Debug.Log("lineSpacing  " +_tmpData.lineSpacing);
               Debug.Log("paragraphSpacing  " +_tmpData.paragraphSpacing);
               Debug.Log("wordSpacing  " +_tmpData.wordSpacing);
               Debug.Log("currentFontAsset  " +_tmpData.currentFontAsset);
                */

               var isAdded = false;
               foreach (var temp in _tmpDataList)
               {
                   if (temp.guid.Equals(_tmpData.guid)) isAdded = true;
               }

               if (!isAdded)
               {
                   _tmpDataList.Add(_tmpData);
  
               } 
    }
    
    
    
    void OnGUI()
    {
        if (_tmpDataList == null) _tmpDataList = new List<TMPData>();
        
        GUILayout.Label("Searching For Files", EditorStyles.boldLabel);


        if (GUILayout.Button("ClearList"))
        {
            _tmpDataList.Clear();
        }
        
        if (GUILayout.Button("SaveListToFile"))
        {
           var saveFile = Application.persistentDataPath + "/gamedata.json";
           TMPDataContainer container = new TMPDataContainer();
           container.data = _tmpDataList;
           
           var json = JsonUtility.ToJson(container);
           
           File.WriteAllText( saveFile , json );

           Debug.Log("SavingFile to : " + saveFile);
           Debug.Log("Text Count: " + _tmpDataList.Count);
        }

        if (GUILayout.Button("Save All TMP In Resources"))
        {
            List<GameObject> gameObjects = GetAllObjectsInResources().ToList();
            foreach (var obj in gameObjects)
            {
                var allTexts = obj.GetComponentsInChildren<TextMeshProUGUI>();
                foreach (var text in allTexts)
                {
                    if (text.gameObject.GetComponent<UniqueFixedID>() == null)
                        text.gameObject.AddComponent<UniqueFixedID>();
                    
                    AddToList(text);
                }

                
                EditorUtility.SetDirty(obj);
                AssetDatabase.SaveAssets();
            }
            

        }


        if (GUILayout.Button("Save All TMP In Opened Scene"))
        {
            var allMeshProTexts = GetAllObjectsOnlyInScene();
         
           foreach (var textScript in allMeshProTexts)
           {
               TextMeshProUGUI textMeshProUGUI = textScript.GetComponent<TextMeshProUGUI>();

              if(textMeshProUGUI == null) continue;
              
              var gameObject = textScript.gameObject;
              AddToList(textMeshProUGUI);
           }
        }



        //myString = EditorGUILayout.TextField("Text Field", myString);
    }
}