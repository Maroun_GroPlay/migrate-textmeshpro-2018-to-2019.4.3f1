﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;

public class ImportSavedData : EditorWindow
{
    private TMP_FontAsset funktionWebfonSDF;
    private TMP_FontAsset neutrafaceSlabTextBoldSDF;
    private TMP_FontAsset font2;

    
    [MenuItem("Migrate/ImportData")]
    static void Init()
    {
        ImportSavedData window = (ImportSavedData)EditorWindow.GetWindow(typeof(ImportSavedData));
        window.Show();

    }
    
    
    
    private void OnGUI()
    {
        
        
        funktionWebfonSDF = EditorGUILayout.ObjectField(funktionWebfonSDF, typeof(TMP_FontAsset), true) as TMP_FontAsset;
        neutrafaceSlabTextBoldSDF = EditorGUILayout.ObjectField(neutrafaceSlabTextBoldSDF, typeof(TMP_FontAsset), true) as TMP_FontAsset;

        if (GUILayout.Button("Tell Me Which GO has missing Script in Scene"))
        {
            var allObj = SaveTextMeshProEditor.GetAllObjectsOnlyInScene();
            Debug.Log("I am here " + allObj.Count);

            foreach (var Obj in allObj)
            {
                var allScripts = Obj.GetComponentsInChildren<MonoBehaviour>();
                foreach (var sc in allScripts)
                {
                    if (sc == null)
                    {
                        Debug.Log(sc.gameObject.transform.root.gameObject.name + " | " + sc.gameObject.name);
                    }
                }

            }
        }



        if (GUILayout.Button("FixWrapping"))
        {
            List<GameObject> gameObjects = SaveTextMeshProEditor.GetAllObjectsInResources().ToList();
            foreach (var obj in gameObjects)
            {
                
                var allTexts = obj.GetComponentsInChildren<TextMeshProUGUI>();
                foreach (var text in allTexts)
                {
                    TextMeshProUGUI textMeshProUGUI = text.gameObject.GetComponent<TextMeshProUGUI>();

                    if (textMeshProUGUI == null) continue;

                    textMeshProUGUI.enableWordWrapping = true;
                    EditorUtility.SetDirty(textMeshProUGUI.gameObject);
                    AssetDatabase.SaveAssets();
                    
                }
            }
            
            
            
        }




        if (GUILayout.Button("ImportData"))
        {
            var saveFile = Application.persistentDataPath + "/gamedata.json";
            var file = File.ReadAllText(saveFile);
            var tmpDataContainer = JsonUtility.FromJson<TMPDataContainer>(File.ReadAllText(saveFile));
            List<TMPData> data = tmpDataContainer.data;
            //funktion-webfont SDF 1
            //NeutrafaceSlabText-Bold SDF 1

            List<GameObject> gameObjects = SaveTextMeshProEditor.GetAllObjectsInResources().ToList();
            
            
            foreach (var obj in gameObjects)
            {
                
                var allTexts = obj.GetComponentsInChildren<UniqueFixedID>();
                foreach (var text in allTexts)
                {
                    UniqueFixedID uniqueFixedId = text.gameObject.GetComponent<UniqueFixedID>();

                    if (uniqueFixedId == null) continue;
                    
                    var gameObject = uniqueFixedId.gameObject;
                    if(gameObject.GetComponent<TextMeshProUGUI>() != null) continue;
                    
                    AddTMP(gameObject,uniqueFixedId, data);
                    EditorUtility.SetDirty(gameObject);
                    Debug.Log("gameObject: " + gameObject.transform.root.gameObject + " I am at:  "+ gameObject.name);
                    AssetDatabase.SaveAssets();
                    
                }

                

            }
            
        }
        
    }

    private void AddTMP(GameObject gameObject,UniqueFixedID fixedID,  List<TMPData> data)
    {



        //GameObjectUtility.RemoveMonoBehavioursWithMissingScript(gameObject);
        

        foreach (var tmpData in data)
        {
            if (fixedID.m_ID.Equals(tmpData.guid))
            {

                TextMeshProUGUI newTextMeshPro = gameObject.AddComponent<TextMeshProUGUI>();
                
 
                newTextMeshPro.color = tmpData.color;
                newTextMeshPro.margin = tmpData.margin;
                newTextMeshPro.text = tmpData.text;
                newTextMeshPro.alignment = tmpData.alignmentOptions;
                newTextMeshPro.autoSizeTextContainer = tmpData.autoSize;
                newTextMeshPro.characterSpacing = tmpData.characterSpacing;
                newTextMeshPro.fontSize = tmpData.fontSize;
                newTextMeshPro.lineSpacing = tmpData.lineSpacing;
                newTextMeshPro.paragraphSpacing = tmpData.paragraphSpacing;
                newTextMeshPro.wordSpacing = tmpData.wordSpacing;
                if (tmpData.currentFontAsset.Contains("funktion-webfont SDF"))
                {
                    newTextMeshPro.font = funktionWebfonSDF;
                }
                else
                {
                    newTextMeshPro.font = neutrafaceSlabTextBoldSDF;
                }

                break;
            }

        }
        DestroyImmediate(fixedID, true);

    }
}
