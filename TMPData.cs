﻿using System;
using TMPro;
using UnityEngine;




[Serializable]
public struct TMPData 
{
        public string guid;
        
        public string text;
        public float fontSize;
        public string currentFontAsset;
        public bool autoSize;
        public Color color;
        
        public float characterSpacing;
        public float wordSpacing;
        public float lineSpacing;
        public float paragraphSpacing;
        
        public TextAlignmentOptions alignmentOptions;
        public Vector4 margin;
}
