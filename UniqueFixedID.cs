﻿using System;
using UnityEngine;

public class UniqueFixedID : MonoBehaviour
{
    [SerializeField] public string m_ID = "";



    private void GenerateID()
    {
        if (m_ID.Equals(""))
        {
            m_ID = Guid.NewGuid().ToString();
        }
    }

    private void OnValidate()
    {
        GenerateID();
    }

    private void OnEnable()
    {
        GenerateID();
    }

    // Start is called before the first frame update
    void Start()
    {
        GenerateID();
    }

}
